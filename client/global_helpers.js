UI.registerHelper('userFirstName', () => {
  if (Meteor.user()){
    return Meteor.user().services.facebook.first_name;
  }
});

UI.registerHelper('userPicture', () => {
  if(Meteor.user() && Meteor.user().profile) {
    return Meteor.user().profile.picture;
  }
});

UI.registerHelper('startSign', (birthday) => {
  return startSign(birthday);
});

UI.registerHelper('users', () => {
  if (Meteor.user() && Meteor.users.find().fetch().length > 1) {
    return Meteor.users.find({ _id: { $ne: Meteor.userId() } });
  }
  return false;
});

UI.registerHelper('equals', (v1, v2) => {
  return (v1 === v2);
});

UI.registerHelper('notEquals', (v1, v2) => {
  return (v1 !== v2);
});
