Meteor.startup(function() {
  // init hooks event system
  Hooks.init();
})

Hooks.onLoggedIn = function () {
  FlowRouter.go("match_route")
}

Hooks.onLoggedOut = function () {
  FlowRouter.go("login_route")
}
