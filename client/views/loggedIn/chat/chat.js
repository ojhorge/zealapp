// import { Chats } from '../../../../collections/chats.js';
// import { Matches } from '../../../../collections/matches.js';
// import { Users } from '../../../../collections/users.js';

Template.chatList.helpers({
  chats: () => {
    if (Meteor.user()) {
      return Chats.find({$or: [{ user: Meteor.userId() }, { match: Meteor.userId() } ]});
    }
  },

  user: (matchId) => {
    if (Meteor.user()) {
      let match = Matches.findOne({ _id: matchId });
      console.log(match);
      if (Meteor.userId == match.user){
        return Meteor.user().profile.firstname;
      }
    }
  },

  other: (matchId) => {
    if (Meteor.user()) {
      let match = Matches.findOne({ _id: matchId });
      console.log(match);
      if (Meteor.userId != match.match_id) {
        return Users.findOne({ _id: match.match_id }).profile.firstname;
      }
    }
  }

});
