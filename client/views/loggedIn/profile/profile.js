Template.profile.events({
  'click #logout': function(e) {
    Meteor.logout()
  }
})

Template.profile.helpers({
  profile: function() {
    return Meteor.user().profile;
  }
})
