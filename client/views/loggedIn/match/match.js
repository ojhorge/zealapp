Template.match.onCreated(function bodyOnCreated() {
  this.state = new ReactiveDict();
  Meteor.subscribe('matches');
});

Template.match.helpers({
    currentMatch: function() {
        return Session.get("currentMatch");
    },

    userMatches: (user) => {
        check(user, Object);

        let userStarSign = user.profile.starsign;
        let userMatches = starSigns[Meteor.user().profile.starsign].matches;

        if ($.inArray(userStarSign, userMatches) > -1) {
            Meteor.call('matches.add', user._id);
            return true;
        }
        return false;
    }
})

Template.match.events({
    'click #generateMatch': function(e) {
        Session.set("currentMatch", "your star sign matches with: " + starSigns[Meteor.user().profile.starsign].matches);
    }
})
