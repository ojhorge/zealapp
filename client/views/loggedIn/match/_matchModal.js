Template.matchModal.rendered = () => {
  event.preventDefault();
  $('#MatModal-container').show(0,function(){
    $(this).addClass('show-MatModal');
    setTimeout(function(){
      $('#MatModal-container').addClass('seeModal');
    },250);
  });

  $('#close-MatModal').click(function(e){
    e.preventDefault();
    closeMatModal();
  });

  function closeMatModal(){
    $('#MatModal-container').removeClass('seeModal show-MatModal');
    setTimeout(function(){
      $('#MatModal-container').hide(150);
    },550);
  };
};

Template.matchModal.events({
    'click [data-id=chat]': function() {
        FlowRouter.go('chatList_route');
    }
});
