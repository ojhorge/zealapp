Template.login.events({
    'click #loginFacebook': function(e, t) {
        Meteor.loginWithFacebook({
            requestPermissions: ['user_birthday', 'public_profile', 'user_photos']
        }, function(err, more) {
          FlowRouter.go('/')
        })
    }
});

