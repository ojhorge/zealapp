import { ServiceConfiguration } from 'meteor/service-configuration'

// Set up login services
Meteor.startup(function() {
  // Add Facebook configuration entry
  ServiceConfiguration.configurations.upsert(
    { service: "facebook" },
    {
      $set: {
        appId: Meteor.settings.private.facebook.appId,
        secret: Meteor.settings.private.facebook.secret,
        loginStyle: "redirect"
      }
    },
    { upsert: true }
  );
  
  
  Hooks.onCreateUser = function (userId) {
    var user = Meteor.users.findOne(userId);

    if (user.hasOwnProperty('services') && user.services.hasOwnProperty('facebook')) {
      var result = Meteor.http.get('https://graph.facebook.com/v2.4/' + user.services.facebook.id + '?access_token=' + user.services.facebook.accessToken + '&fields=first_name, birthday, email, gender');

      // get profile picture
      var request = require('request');
      var stream = profilePictures.upsertStream(
        {
           _id: new Meteor.Collection.ObjectID(),
           filename: "profile_" + user.services.facebook.id,
           contentType: 'image/jpg'
        },
        function (err, fileObj) {
           if (err) {
               console.log(err);
           } else {
               Meteor.users.update(userId, {$set:
                 {
                   "profile.firstname": result.data.first_name,
                   "profile.birthday": result.data.birthday,
                   "profile.starsign": startSign(new Date(result.data.birthday)),
                   "profile.gender": result.data.gender,
                   "profile.email": result.data.email,
                   "profile.picture": '/gridfs/profilePictures/' + fileObj.md5
                 }
               });
           }
        }
      );

      stream.on('finish', function () { /* Perform any actions after write... */});

      request("https://graph.facebook.com/v2.8/" + user.services.facebook.id + "/picture?width=800&height=800")
         .on('error', function(err) { /* Handle error */ })
         .pipe(stream);
      
    }

  }


});
