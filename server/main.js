import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
});

Meteor.methods({
  //placeholder
  eventsOnHooksInit : function(){},
  log : function(text){
    console.log("client: " + text)
  }
});
