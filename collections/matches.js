import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Matches = new Mongo.Collection("matches");

if (Meteor.isServer) {
  Meteor.publish('matches', function matchesPublication() {
    return Matches.find({
      $or: [
        { user: this.userId },
        { match_id: this.userId }
      ]
    });
  });
}

Meteor.methods({
    'matches.add'(userMatch){
        check(userMatch, String);

        // Make sure the user is currently logged in
        if (! this.userId){
            throw new Meteor.Error('not-authorized');
        }

        // verify if there are any matches from both sides
        var anyMatch = Matches.find({user: this.userId, match_id: userMatch}).fetch();
        anyMatch = anyMatch.concat(Matches.find({user: userMatch, match_id: this.userId}).fetch());

        if (anyMatch.length == 0) {
            var matchId = Matches.insert({
                user: this.userId,
                user_notified: false,
                match_id: userMatch,
                match_notified: false,
                state: "active",
                createdAt: new Date(),
                maxchar: 15000
            });

            Meteor.call('matches.notified', matchId, this.userId);
            Meteor.call('chats.create', matchId);
        }

    },
    'matches.update'(matchId, state){
        check(matchId, String);
        check(state, String);

        Matches.update(matchId, { $set: { state: state, modified_at: new Date() } });
    },

    'matches.notified'(matchId, userId){
        check(matchId, String);
        check(userId, String);

        let match = Matches.find({ _id: matchId });

        if (match.user == userId) {
            if (match.user_notified != true) {
                Matches.update(matchId, { $set: { user_notified: true, modified_at: new Date() } });
            }
        }
        if (match.match_id == userId) {
            if (match.match_notified != true) {
                Matches.update(matchId, { $set: { match_notified: true, modified_at: new Date() } });
            }
        }
    },


});
