import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Messages = new Mongo.Collection('messages');

Meteor.methods({
  'messages.add'(chatId, userId, message) {
    check(chatId, String);
    check(userId, String);
    check(message, String);

    if (!Meteor.user()) {
      throw new Meteor.Error('not-authorized');
    }

    let chat = Chats.findOne({ _id: chatId });

    if (!chat) {
      throw new Meteor.Error('not-found');
    } else {
      Messages.insert({
        chat_id: chatId,
        owner_id: userId,
        message: message,
        createdAt: new Date(),
        modifiedAt: new Date()
      });
    }

  },

  'messages.edit'() {

  },

  'messages.remove'(messageId) {
    check(messageId, String);

    if (!Meteor.user()) {
      throw new Meteor.Error('not-authorized');
    }

    let message = Messages.findOne({ _id: message});
    Messages.remove(messageId);
  }
});
