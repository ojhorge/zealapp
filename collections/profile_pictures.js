profilePictures = new FileCollection('profilePictures',
  { resumable: true,    // Enable built-in resumable.js chunked upload support
    http: [             // Define HTTP route
      { method: 'get',  // Enable a GET endpoint
        path: '/:md5',  // this will be at route "/gridfs/profilePictures/:md5"
        lookup: function (params, query) {  // uses express style url params
          return { md5: params.md5 };       // a query mapping url to myFiles
}}]});

if(Meteor.isServer) {
  profilePictures.allow({
    // The creator of a file owns it. UserId may be null.
    insert: function(userId, file) {
      // Assign the proper owner when a file is created
      return true;
    },
    // Only owners can remove a file
    remove: function(userId, file) {
      if (user_in_roles(userId, ['admin', 'info_admin'])) {
          return true;
      }
      // Only owners can delete
      return (userId === file.metadata.owner);
    },
    // Only owners can retrieve a file via HTTP GET
    read: function(userId, file) {
      return true;
    },
    // This rule secures the HTTP REST interfaces' PUT/POST
    // Necessary to support Resumable.js
    write: function(userId, file, fields) {
      // Only owners can upload file data
      return (userId === file.metadata.owner);
    }
  });
}
