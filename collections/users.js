Schema = {};

Schema.UserProfile = new SimpleSchema({
  "firstname": {
    type: String,
    optional: true
  },
  "email": {
    type: String,
    optional: true
  },
  "birthday": {
    type: Date,
    optional: true
  },
  "gender": {
    type: String,
    optional: true
  },
  "picture": {
    type: String,
    optional: true
  },
  "starsign": {
    type: String,
    optional: true
  },
});

Meteor.users.allow({
  update: function(userId, doc) {
    return  (userId === doc._id)
  }
})

Schema.User = new SimpleSchema({
  createdAt: {
    type: Date
  },
  profile: {
    type: Schema.UserProfile,
    optional: true
  },
  // Make sure this services field is in your schema if you're using any of the accounts packages
  services: {
    type: Object,
    optional: true,
    blackbox: true
  },
  // In order to avoid an 'Exception in setInterval callback' from Meteor
  heartbeat: {
    type: Date,
    optional: true
  }
});

Meteor.users.attachSchema(Schema.User);
