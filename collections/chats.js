import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Matches } from './matches.js';

export const Chats = new Mongo.Collection("chats");

Meteor.methods({
  'chats.create'(matchId) {
    check(matchId, String);

    if (!Meteor.user()) {
      throw new Meteor.Error('not-authorized');
    }

    let match = Matches.findOne({ _id: matchId });
    
    Chats.insert({
      matchId: matchId,
      user: match.user,
      other_user: match.match_id,
      createdAt: new Date(),
      modifiedAt: new Date(),
      messages: []
    });
  },

  'chats.remove'(chatId) {
    check(chatId, String);

    if (!Meteor.user()) {
      throw new Meteor.Error('not-authorized');
    }

    Chats.remove(chatId);
  },

  'chats.addMessage'(chatId, messageId) {
    check(chatId, String);
    check(messageId, String);

    if (!Meteor.user()) {
      throw new Meteor.Error('not-authorized');
    }

    // TODO: Add the massage-id to the chat and update the modifiedAt value
  }

});
