
starSigns = {
  'aquarius': {matches: ['gemini', 'libra']},
  'pisces': {matches: ['cancer', 'scorpio']},
  'aries': {matches: ['leo', 'sagittarius']},
  'taurus': {matches: ['virgo', 'capricorn']},
  'gemini': {matches: ['libra', 'aquarius']},
  'cancer': {matches: ['scorpio', 'pisces']},
  'leo': {matches: ['aries', 'sagittarius']},
  'virgo': {matches: ['taurus', 'capricorn']},
  'libra': {matches: ['aquarius', 'gemini']},
  'scorpio': {matches: ['pisces', 'cancer']},
  'sagittarius': {matches: ['aries', 'leo']},
  'capricorn': {matches: ['taurus', 'virgo']}
}


startSign = function(birthday) {
  var zod_signs = ["capricorn" , "aquarius", "pisces", "aries",
  "taurus", "gemini", "cancer", "leo", "virgo", "libra", "scorpio",
  "sagittarius"];
  var day = birthday.getDate();
  var sign = "";
  switch(birthday.getMonth())
  {
  	case 0: {
  			 if(day < 20)
  		 		sign = zod_signs[0];
  			 else
  		 		sign = zod_signs[1];
  		    }break;
  	case 1: {
  			 if(day < 19)
  		 		sign = zod_signs[1];
  			 else
  		 		sign = zod_signs[2];
  			}break;
  	case 2: {
  			 if(day < 21)
  			 	sign = zod_signs[2];
  			 else
  			 	sign = zod_signs[3];
  			}break;
  	case 3: {
  			 if(day < 20)
  		 		sign = zod_signs[3];
  			 else
  		 		sign = zod_signs[4];
  			}break;
  	case 4: {
  			 if(day < 21)
  		 		sign = zod_signs[4];
  			 else
  		 		sign = zod_signs[5];
  			}break;
  	case 5: {
  			 if(day < 21)
  		 		sign = zod_signs[5];
  			 else
  		 		sign = zod_signs[6];
  			}break;
  	case 6: {
  			 if(day < 23)
  		 		sign = zod_signs[6];
  			 else
  		 		sign = zod_signs[7];
  			}break;
   	case 7: {
  			 if(day < 23)
  		 		sign = zod_signs[7];
  			 else
  		 		sign = zod_signs[8];
  			}break;
  	case 8: {
  			 if(day < 23)
  		 		sign = zod_signs[8];
  			 else
  		 		sign = zod_signs[9];
  			}break;
  	case 9: {
  			 if(day < 23)
  		 		sign = zod_signs[9];
  			 else
  		 		sign = zod_signs[10];
  			}break;
  	case 10: {
  			 if(day < 22)
  		 		sign = zod_signs[10];
  			 else
  		 		sign = zod_signs[11];
  			}break;
  	case 11: {
  			 if(day < 22)
  		 		sign = zod_signs[11];
  			 else
  		 		sign = zod_signs[0];
  			}break;
  }
  return sign;
}