FlowRouter.route('/login', {
  name: "login_route",
  triggersEnter: [function(context, redirect) {
    if (Meteor.userId()) {
      redirect('match_route');
    }
  }],
  action: function(params, queryParams) {
    BlazeLayout.render('landingLayout', {
      main: "login",
    });
  }
});

var loggedIn = FlowRouter.group({
  triggersEnter: [function(context, redirect) {
    if (!Meteor.userId()) {
      redirect('login_route');
    }
  }]
});

// loggedIn.route('/', {
//   name: "home_route",
//   action: function(params, queryParams) {
//     BlazeLayout.render('masterLayout', {
//       main: "home",
//       nav: "nav"
//     });
//   }
// });

loggedIn.route('/profile', {
  name: "profile_route",
  action: function(params, queryParams) {
    BlazeLayout.render('masterLayout', {
      main: "profile",
      nav: "nav"
    });
  }
});

loggedIn.route('/', {
  name: "match_route",
  action: function(params, queryParams) {
    BlazeLayout.render('masterLayout', {
      main: "match",
      nav: "nav"
    });
  }
});

loggedIn.route('/chats', {
    name: "chatList_route",
    action: function(params, queryParams) {
        BlazeLayout.render('masterLayout', {
            main: "chatList",
            nav: "nav"
        });
    }
});
